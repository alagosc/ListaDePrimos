package cl.ubb.ListaPrimos;

import java.util.ArrayList;
import java.util.List;

public class NumerosPrimos {

	public static List<Integer> generarLista(int i) {
		// TODO Auto-generated method stub
		List<Integer> listaPrimos = new ArrayList<Integer>();
		int primo = 0;
	
		if(i == 1){
			listaPrimos.add(i);
		}
		
		if(i == 2){
			listaPrimos.add(1);
			listaPrimos.add(2);
		}

		if (i > 2){
			for (int x = 1; x <= i; x ++){
				primo = 0;
				for (int j=1; j<=x; j++){
					if(x%j == 0){
						primo ++;
					}
				}
				if(primo == 2){
					listaPrimos.add(x);
				}
			}
		}
		
			
		
	/*	if(i == 1){
			listaPrimos.add(i);
		}
		
		if(i == 2){
			listaPrimos.add(1);
			listaPrimos.add(2);
		}
		
		if(i == 3){
			listaPrimos.add(1);
			listaPrimos.add(2);
			listaPrimos.add(3);
		}*/
		
		return listaPrimos;
	}
	
}
