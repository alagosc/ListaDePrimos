package cl.ubb.ListaPrimos;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ListaPrimosTest {

	@Test
	public void GeneraListaNumerosPrimosHastaCero() {
		/*arrange*/
		List<Integer> lista = new ArrayList<Integer>();
		List<Integer> listaEsperada = new ArrayList<Integer>();
		
		
		/*act*/
		lista = NumerosPrimos.generarLista(0);
		
		/*assert*/
		assertEquals(listaEsperada, lista);
	}

	@Test
	public void GeneraListaNumerosPrimosHastaUno() {
		/*arrange*/
		List<Integer> lista = new ArrayList<Integer>();
		List<Integer> listaEsperada = new ArrayList<Integer>();
		listaEsperada.add(1);
		
		/*act*/
		lista = NumerosPrimos.generarLista(1);
		
		/*assert*/
		assertEquals(listaEsperada, lista);
	}
	
	@Test
	public void GeneraListaNumerosPrimosHastaDos() {
		/*arrange*/
		List<Integer> lista = new ArrayList<Integer>();
		List<Integer> listaEsperada = new ArrayList<Integer>();
		listaEsperada.add(1);
		listaEsperada.add(2);
		/*act*/
		lista = NumerosPrimos.generarLista(2);
		
		/*assert*/
		assertEquals(listaEsperada, lista);
	}
	
	@Test
	public void GeneraListaNumerosPrimosHastaTres() {
		/*arrange*/
		List<Integer> lista = new ArrayList<Integer>();
		List<Integer> listaEsperada = new ArrayList<Integer>();
		//listaEsperada.add(1);
		listaEsperada.add(2);
		listaEsperada.add(3);
		/*act*/
		lista = NumerosPrimos.generarLista(3);
		
		/*assert*/
		assertEquals(listaEsperada, lista);
	}
	
	@Test
	public void GeneraListaNumerosPrimosHastaCuatro() {
		/*arrange*/
		List<Integer> lista = new ArrayList<Integer>();
		List<Integer> listaEsperada = new ArrayList<Integer>();
		//listaEsperada.add(1);
		listaEsperada.add(2);
		listaEsperada.add(3);
		/*act*/
		lista = NumerosPrimos.generarLista(4);
		
		/*assert*/
		assertEquals(listaEsperada, lista);
	}
		
}
